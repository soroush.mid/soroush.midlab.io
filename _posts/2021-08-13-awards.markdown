---
layout: post
title:  "Awards"
date:   2021-08-13 00:00:00 -0300
categories: post
---
- Winner of the best M.Sc. IoT-related thesis award among all competitors in 5th International Conference on Internet of Things and Applications
![best M.Sc. IoT-related thesis](https://s4.uupload.ir/files/iotcert-best_thesis-_ut2s.jpg)

- Winner of the best paper award in the High Performance Computing and Big Data Analytics (TopHPC)
congress
![best paper award](https://s4.uupload.ir/files/best_paper_certificate-_aqwi.jpg)

- Ranked 3rd among M.Sc. Computer Engineering students at Ferdowsi University of Mashhad

- Ranked 2nd among B.Sc. Computer Engineering students and got accepted for M.Sc. at Ferdowsi University without entrance qualification exam

